from django.http.response import Http404, HttpResponseRedirect
from django.shortcuts import redirect, render
from django.http import HttpResponse
from django.utils import timezone
from project.models import login_info, account_data
from django.contrib import messages

global Now_login_id
Now_login_id=None
# Create your views here.
def index(request):
    return render(request,'project/homepage.html')

def login(request):
     if request.method == 'POST':
          if 'login' in request.POST:
               # loginがクリックされた場合の処理
               global Now_login_id
               Now_login_id =request.POST['ID']
               id =request.POST['ID']
               pas =request.POST['password']
               try:
                  data = login_info.objects.get(id=id)
               except login_info.DoesNotExist:
                    print('login-error')
                    messages.error(request,'存在しないアカウント名です')
                    return HttpResponseRedirect('login')
               else:
                   if id == data.id and pas == data.pas :
                        print('Account dose exist')
                        try:
                            check = account_data.objects.get(account_id=data)
                        except account_data.DoesNotExist:
                            return render(request,'project/profile.html')
                        else:
                            data = account_data.objects.order_by('grade')
                            ac = account_data.objects.get(account_id=Now_login_id)
                            data = {
                                'data':data,
                                'usericon':ac.images,
                                'account':ac.nickname
                            }
                            return render(request,'project/matching.html',data)
                   else:
                       print('login-error')
                       messages.error(request,'正しいアカウント名とパスワードを入力してください')
                       return HttpResponseRedirect('login')
     return render(request,'project/login.html')

def dm(request, user_id):
    data = {
        "name": "Alice",
        "messages": [
            {
                "id": 1,
                "name": "Alice",
                "body": "こんにちは", 
                "posted_at": timezone.now # option
            },
            {
                "id": 2,
                "name": "Tarou",
                "body": "どうも", 
                "posted_at": timezone.now # option
            }
        ]
    }
    return render(request,'project/dm.html', data)


def profile(request):
    if request.method == 'POST':
        if 'submit' in request.POST:
            nickname=request.POST['nickname']
            age=request.POST['age']
            favorite=request.POST['favorite']
            intro=request.POST['intro']
            grade=request.POST.getlist('grade')
            gender=request.POST.getlist('gender')
            hard=request.POST.getlist('hard')
            icon= request.FILES['icon']
            sub_key = login_info.objects.get(id=Now_login_id)

            #ここからデータベースにデータを入力する動作
            account = account_data(nickname=nickname,age=age,grade=grade,gender=gender,favorite=favorite,hard=hard,intro=intro,images=icon,account_id=sub_key)
            account.save()
            #ここから確認画面へ遷移する
            check_data = account_data.objects.get(account_id=sub_key)
            gender = check_data.gender.replace('\'','').replace('[','').replace(']','')
            grade = check_data.grade[2]
            hard = check_data.hard.replace('\'','').replace('[','').replace(']','')
            data = {
                'nickname':check_data.nickname,
                'age':check_data.age,
                'favorite':check_data.favorite,
                'intro':check_data.intro,
                'grade':grade,
                'gender':gender,
                'hard':hard,
                'icon':check_data.images,
            }
            return render(request,'project/check.html',data)
    return render(request,'project/profile.html')

def account(request):
    empty = None
    data = {
        "empty":empty,
        }
    if request.method == 'POST':
        if 'reset_pass' in request.POST:
            # リセットボタンがクリックされた場合の処理
            reset_pass = True
            data = {
                "reset_pass":reset_pass,
            }
        if 'creat_new' in request.POST:
            # 新規作成ボタンがクリックされた場合の処理
            creat_new = True
            data = {
                "creat_new":creat_new,
            }
        if 'reset' in request.POST:
            # パスワードの再設定が行われた場合の処理
            global account_id
            account_id =request.POST['ID']
            secret =request.POST['secret_key']
            try:
                  data = login_info.objects.get(id=account_id)
            except login_info.DoesNotExist:
                    print('account_DoesNotExist')
                    messages.error(request,'存在しないアカウント名です')
                    reset_pass = True
                    data={"reset_pass":reset_pass}
                    return render(request,'project/account.html',data)
            else:
                   if account_id == data.id and secret == data.secret :
                        print('Account dose exist')
                        password = data.pas
                        reset_pass = True
                        data = {
                            "reset_pass":reset_pass,
                            "password":password,
                        }
                        return render(request,'project/account.html',data)
                   else:
                       print('error')
                       messages.error(request,'正しいアカウント名と秘密の合言葉を入力してください')
                       reset_pass = True
                       data={"reset_pass":reset_pass}
                       return render(request,'project/account.html',data)
        if "resubmit-pass" in request.POST:
            new_pass = request.POST['renew_pass']
            data = login_info.objects.get(id=account_id)
            data.pas = new_pass
            data.save()
            return HttpResponseRedirect('login')
            
        if 'creat' in request.POST:
            #新規作成画面でアカウント作成ボタンがクリックされた場合の処理
            new_account = request.POST['new_account']
            new_password = request.POST['password']
            new_secret = request.POST['secret_key']
            if not (new_account == '' or new_password == '' or new_secret== ''):
                try:
                    check_duplicate = login_info.objects.get(id=new_account)
                except login_info.DoesNotExist:
                    account = login_info(id=new_account,pas=new_password,secret=new_secret)
                    account.save()
                else:
                    creat_new = True
                    false = True
                    data = {
                    "creat_new":creat_new,
                    "false":false
                    }
                    messages.error(request,'そのアカウント名は既に使用されています')
                    return render(request,'project/account.html',data)
            else:
                creat_new = True
                false = True
                data = {
                    "creat_new":creat_new,
                    "false":false
                }
                messages.error(request,'入力に誤りがあります')
                return render(request,'project/account.html',data)
            messages.info(request,'アカウントが新規作成されました')
            success = True
            creat_new = True
            data = {
                "success":success,
                "creat_new":creat_new,
            }            
    return render(request,'project/account.html',data)



def check(request):
    if request.method == 'POST':
        if 'no' in request.POST:
            check_data = account_data.objects.get(account_id=Now_login_id)
            gender = check_data.gender.replace('\'','').replace('[','').replace(']','')
            grade = check_data.grade[2]
            hard = check_data.hard.replace('\'','').replace('[','').replace(']','').replace(',','').replace(' ','')
            data = {
                'nickname':check_data.nickname,
                'age':check_data.age,
                'favorite':check_data.favorite,
                'intro':check_data.intro,
                'grade':grade,
                'gender':gender,
                'hard':hard,
            }
            return render(request,'project/profile.html',data)
        if 'ok' in request.POST:
            data = account_data.objects.order_by('grade')
            ac = account_data.objects.get(account_id=Now_login_id)
            data = {
                    'data':data,
                    'usericon':ac.images,
                    'account':ac.nickname
                    }
            return render(request,'project/matching.html',data)
            
    return render(request,'project/check.html')

def matching(request):
    if request.method == 'POST':
        if 'profile_setting' in request.POST:
            check_data = account_data.objects.get(account_id=Now_login_id)
            gender = check_data.gender.replace('\'','').replace('[','').replace(']','')
            grade = check_data.grade[2]
            hard = check_data.hard.replace('\'','').replace('[','').replace(']','').replace(',','').replace(' ','')
            data = {
                'nickname':check_data.nickname,
                'age':check_data.age,
                'favorite':check_data.favorite,
                'intro':check_data.intro,
                'grade':grade,
                'gender':gender,
                'hard':hard,
            }
            return render(request,'project/profile.html',data)
        if 'delete' in request.POST:
            delete_account = login_info.objects.get(id=Now_login_id)
            delete_account.delete()
            return render(request,'project/homepage.html')
        if 'sort_by_grade-up' in request.POST:
            ac = account_data.objects.get(account_id=Now_login_id)
            data = account_data.objects.order_by('grade')
            data = {
                    'data':data,
                     'usericon':ac.images,
                     'account':ac.nickname,
                    }
            return render(request,'project/matching.html',data)
        if 'sort_by_grade-down' in request.POST:
            ac = account_data.objects.get(account_id=Now_login_id)
            data = account_data.objects.order_by('-grade')
            data = {
                    'data':data,
                     'usericon':ac.images,
                     'account':ac.nickname,
                    }
            return render(request,'project/matching.html',data)
        if 'sort_by_age-up' in request.POST:
            ac = account_data.objects.get(account_id=Now_login_id)
            data = account_data.objects.order_by('age')
            data = {
                    'data':data,
                     'usericon':ac.images,
                     'account':ac.nickname,
                    }
            return render(request,'project/matching.html',data)
        if 'sort_by_age-down' in request.POST:
            ac = account_data.objects.get(account_id=Now_login_id)
            data = account_data.objects.order_by('-age')
            data = {
                    'data':data,
                     'usericon':ac.images,
                     'account':ac.nickname,
                    }
            return render(request,'project/matching.html',data)
        if 'sort_by_favorite-up' in request.POST:
            ac = account_data.objects.get(account_id=Now_login_id)
            data = account_data.objects.order_by('favorite')
            data = {
                    'data':data,
                     'usericon':ac.images,
                     'account':ac.nickname,
                    }
            return render(request,'project/matching.html',data)
        if 'sort_by_favorite-down' in request.POST:
            ac = account_data.objects.get(account_id=Now_login_id)
            data = account_data.objects.order_by('-favorite')
            data = {
                    'data':data,
                    'usericon':ac.images,
                    'account':ac.nickname,
                    }
            return render(request,'project/matching.html',data)