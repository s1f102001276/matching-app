from django.db import models
from django.core.validators import MinLengthValidator

# Create your models here.
class login_info(models.Model):
    id = models.CharField(max_length=16,primary_key=True)
    pas = models.CharField(max_length=10)
    secret = models.CharField(max_length=20)

    def __str__(self):
        return self.id & self.pas

class account_data(models.Model):
    nickname = models.CharField(max_length=20,validators=[MinLengthValidator(2)],primary_key=True)
    age = models.CharField(max_length=3,validators=[MinLengthValidator(2)])
    grade = models.CharField(max_length=1)
    gender = models.CharField(max_length=3)
    favorite = models.CharField(max_length=30,validators=[MinLengthValidator(3)])
    hard = models.CharField(max_length=15)
    intro = models.TextField(validators=[MinLengthValidator(6)])
    images = models.ImageField(upload_to='images/')
    account_id = models.ForeignKey(login_info,on_delete=models.CASCADE)