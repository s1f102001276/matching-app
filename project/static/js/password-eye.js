$(function() {
    var $passwords = $('[id$=pass]');
    var $passeyes = $('[id$=pass-eye]');
    var $pass_eye_fields = $('[id$=pass-eye-field]');
    
    $($pass_eye_fields).on('click', function() {

        $($passeyes).toggleClass("fa-eye fa-eye-slash");

        if ($($passwords).attr('type') == "password") {
            $($passwords).attr('type','text');
        } else {
            $($passwords).attr('type','password');
        }
    });
});