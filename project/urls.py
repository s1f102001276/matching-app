from django.urls import path
from . import views


urlpatterns = [
    path('', views.index, name='index'),
    path('login', views.login, name='login'),
    path('dm/<int:user_id>', views.dm, name='dm'),
    path('matching', views.matching, name='matching'),
    path('profile', views.profile, name='profile'),
    path('account',views.account, name='account'),
    path('check',views.check,name='check')
]

