from django.template.defaultfilters import register



@register.filter
def cut(value):
    return value.replace('\'','').replace('[','').replace(']','')